<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Designed by Shamiul">
  <script src="https://cdn.tailwindcss.com"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body class="md:h-screen lg:h-screen h-[100%] min-w-[100%] bg-gradient-to-r from-slate-900 to-indigo-900" style="min-width: 375px; height: 100vh;">
  
  <!-- <main class="md:h-screen lg:h-screen h-[100%] min-w-[100%] bg-gradient-to-r from-slate-900 to-indigo-900" style="height: 100vh;"> -->
  <main>
    
    <header class="bg-slate-700"> 
      @include('header')
    </header>

    
    <div class="pl-16 pr-16 pt-12 d-flex justify-content-center">
      <div class="flex justify-center mb-5">
        <div class="text-center w-full md:w-1/2 p text-white">
          <h4 class="text-2xl md:text-3xl font-bold uppercase mb-2">{{$place_description->name}}</h4>
          <p class="md:text-lg text-base font-medium">Address: {{isset($place_description->location->address)?$place_description->location->address:null}},{{ isset($place_description->location->locality)?$place_description->location->locality:null }},{{ isset($place_description->location->region)?$place_description->location->region:null }}</p>
          <p class="md:text-lg text-base font-medium">ZIPCODE: {{isset($place_description->location->postcode)?$place_description->location->postcode:null}}</p>
        </div>
      </div>
    </div>  
    <div class="pl-16 pr-16 pt-12 d-flex justify-content-center">
      
      
      
      <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="width: 800px;">
        
        <div class="">
          @if(count($place_photos) == 0)
           <p class="md:text-lg text-base font-medium text-white">Image dosen't Found.......</p>
          @else
          @foreach($place_photos as $key=>$photo)
          
            <p class="md:text-lg text-base font-medium text-white">{{substr($photo->created_at,0,10)}}</p>
            <img src="{{$photo->prefix}}{{$photo->width}}x{{$photo->height}}{{$photo->suffix}}" class="d-block w-100" alt="">
            <br>
            
          @endforeach
          @endif
        </div>
        
      </div>

    </div>

  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <script>
    $(document).ready(function () {
      // Show Menu
        $(".nav-list").click(function () {
          $(this).hide();
          $(".nav-close").fadeIn();
          $(".nav-menu").removeClass("hidden");
        });
        // Hide Menu
        $(".nav-close").click(function () {
          $(this).hide();
          $(".nav-list").fadeIn();
          $(".nav-menu").addClass("hidden");
        });
      });
  </script>

</body>

</html>,