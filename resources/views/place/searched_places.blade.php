<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Designed by Shamiul">
  <script src="https://cdn.tailwindcss.com"></script>
  <style>
  #map {
    height: 100%;
  }
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
  </style>
</head>

<body style="min-width: 375px;">
  
  <main class="md:h-screen lg:h-screen h-[100%] min-w-[100%]">
    @include('header')

    <div class="grid md:grid-cols-3 font-sans">
      @include('place.list_map')
    </div>

  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script>
    $(document).ready(function () {
      // Show Menu
        $(".nav-list").click(function () {
          $(this).hide();
          $(".nav-close").fadeIn();
          $(".nav-menu").removeClass("hidden");
        });
        // Hide Menu
        $(".nav-close").click(function () {
          $(this).hide();
          $(".nav-list").fadeIn();
          $(".nav-menu").addClass("hidden");
        });
      });
  </script>

</body>

</html>