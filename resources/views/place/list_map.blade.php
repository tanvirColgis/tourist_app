<script type="text/javascript">
  // declare an object to store the latitutes and longitudes of searched venues
  var locations =new Object();
</script>
      <div class="md:col-span-1 bg-slate-100 shadow-sm overflow-x-auto pt-4 overflow-y-auto h-screen">
        @if(count($places) == 0)
          <p class="text-sm">Nothing Found.......</p>
        @else
        @foreach($places as $key=>$place)
        <a href="{{route('place.detail',[$place->fsq_id])}}" class="bg-white hover:shadow-md mb-2 ease-in duration-300 block" target="_blank">
          <div class="p-5 flex justify-between items-center">
            <div class="pr-4 md:pr-10">
              <h4 class="text-lg font-medium">{{$place->name}}</h4>
              
              <p class="text-sm">Category: {{$place->categories[0]->name}}</p>
            </div>
             
          </div>
        </a>
        <script type="text/javascript">
          // for each loop latitude, longitude and name of the venue is storing in object(locations)

          locations[{{$key}}] = { "lat": "{{$place->geocodes->main->latitude}}", "lng": "{{$place->geocodes->main->longitude}}", 'name': "{{$place->name}}" };
        </script>
        @endforeach
        @endif
      </div>

      <div class="md:col-span-2" id="map">
         
      </div>

<script type="text/javascript">
  
  function initMap() {
    // declare the specific location in the map
    var options={
      zoom:13,
      center:{ lat:{{$lat}}, lng:{{$lan}} },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    
    // map will be put down specific portion(#map) in HTML 
    var map= new google.maps.Map(document.getElementById('map'),options)
 
    // fetching the objet(locations) to get the latitude, longitude and name to add marker
    Object.keys(locations).forEach(function(key) {
        addMarker({ lat:parseFloat(locations[key].lat) , lng:parseFloat(locations[key].lng)},locations[key].name);
    });
      
    // This function is to add marker, info window and click event listener for every venue 
    function addMarker(coords,name){
      var marker= new google.maps.Marker({
          position:coords,
          map:map,
          icon:"https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"
      });

      var infowindow = new google.maps.InfoWindow({
        content: '<h1>'+name+'</h1>'
      });


      marker.addListener('click',function(){
        infowindow.open(map,marker)
      });

    }
  }
</script>
<script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUVMKy3Xsd5Rasx6_7r1YL38OOlnsrzqs&callback=initMap">
</script> 