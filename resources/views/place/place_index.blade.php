<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Designed by Shamiul">
  <script src="https://cdn.tailwindcss.com"></script>
</head>

<body style="min-width: 375px;">
  
  <main class="md:h-screen lg:h-screen h-[100%] min-w-[100%] bg-gradient-to-tl from-slate-900 to-indigo-900">
    <header class="bg-slate-700">
      @include('header')
    </header>

    <div class="font-sans py-5 md:py-12 px-3">
      <div class="flex justify-center mb-5">
        <div class="text-center w-full md:w-1/2 p text-white">
          <h2 class="text-3xl md:text-5xl font-bold uppercase mb-4">Bizmates Tourist Guide</h2>
          <p class="md:text-lg text-base font-medium">Find the best places to eat, drink, shop, or visit in any city in Japan.</p>
        </div>
      </div>
      
      <form id="myForm" action="{{route('search.place')}}" method="POST">
        @csrf
      <div class="flex justify-center mb-4">
        <div class="text-center w-full md:w-1/2 md:flex">
          <div class="relative mb-3 w-full md:w-2/5 md:mr-3">
            <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="topic">
              <option value=""></option>
              <option value="Food">Food</option>
              <option value="Coffee">Coffee</option>
              <option value="Nightlife">Nightlife</option>
              <option value="Fun">Fun</option>
              <option value="Shopping">Shopping</option>
            </select>
            <svg class="fill-current h-4 w-4 text-slate-900 absolute top-4 right-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
          </div>

          <div class="relative mb-3 w-full md:w-2/5 md:mr-3">
          <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="prefecture">
            @foreach($prefectures as $key=>$ken)
              <option value="{{$ken}}" @if($current_city==$ken) selected @endif>{{$ken}}</option>
            @endforeach  
          </select>
          <svg class="fill-current h-4 w-4 text-slate-900 absolute top-4 right-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
          </div>
          <button type="submit" id="submit_form" class="flex w-28 bg-green-600 px-3 py-3 rounded hover:opacity-80 transition text-white mb-3" style="height: 46px;" name="submit">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg> Search
          </button>
        </div>
      </div>

      <div class="flex justify-center">
        <div class="text-center w-full md:w-2/5 p text-white">
          <ul class="text-white grid md:grid-cols-5 grid-cols-3 gap-1">
            <li>
              <a href="#" class="text-center bg-blue-700 flex flex-col items-center py-3 px-2 rounded-tl rounded-bl transition bg-opacity-30 hover:bg-opacity-70 font-medium" onclick="submit_form('Food')">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0" />
                </svg>
                Food
              </a>
            </li>
            <li>
              <a href="#" class="text-center bg-blue-700 flex flex-col items-center py-3 px-2 transition bg-opacity-30 hover:bg-opacity-70 font-medium" onclick="submit_form('Coffee')">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19.428 15.428a2 2 0 00-1.022-.547l-2.387-.477a6 6 0 00-3.86.517l-.318.158a6 6 0 01-3.86.517L6.05 15.21a2 2 0 00-1.806.547M8 4h8l-1 1v5.172a2 2 0 00.586 1.414l5 5c1.26 1.26.367 3.414-1.415 3.414H4.828c-1.782 0-2.674-2.154-1.414-3.414l5-5A2 2 0 009 10.172V5L8 4z" />
                </svg>
                Coffee
              </a>
            </li>
            <li>
              <a href="#" class="text-center bg-blue-700 flex flex-col items-center py-3 px-2 transition bg-opacity-30 hover:bg-opacity-70 font-medium" onclick="submit_form('Nightlife')">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 15.546c-.523 0-1.046.151-1.5.454a2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.701 2.701 0 00-1.5-.454M9 6v2m3-2v2m3-2v2M9 3h.01M12 3h.01M15 3h.01M21 21v-7a2 2 0 00-2-2H5a2 2 0 00-2 2v7h18zm-3-9v-2a2 2 0 00-2-2H8a2 2 0 00-2 2v2h12z" />
                </svg>
                Nightlife
              </a>
            </li>
            <li>
              <a href="#" class="text-center bg-blue-700 flex flex-col items-center py-3 px-2 transition bg-opacity-30 hover:bg-opacity-70 font-medium" onclick="submit_form('Fun')">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01" />
                </svg>
                Fun
              </a>
            </li>
            <li>
              <a href="#" class="text-center bg-blue-700 flex flex-col items-center py-3 px-2 rounded-tr rounded-br transition bg-opacity-30 hover:bg-opacity-70 font-medium" onclick="submit_form('Shopping')">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
                </svg>
                Shopping
              </a>
            </li>
          </ul>
        </div>
      </div>
      <input type="hidden" id="button_topic" name="button_topic" value="">
      </form>
    </div>
  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script>

    $(document).ready(function () {
      // Show Menu
        $(".nav-list").click(function () {
          $(this).hide();
          $(".nav-close").fadeIn();
          $(".nav-menu").removeClass("hidden");
        });
        // Hide Menu
        $(".nav-close").click(function () {
          $(this).hide();
          $(".nav-list").fadeIn();
          $(".nav-menu").addClass("hidden");
        });
      });

    function submit_form(topic){
      $('#button_topic').val(topic)
      $("#submit_form").click()
    }
  </script>

</body>

</html>