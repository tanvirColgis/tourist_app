<div class="bg-slate-600 max-w-screen-xl rounded-lg md:flex lg:flex basis-full">
        
          <!-- Monday -->
          <div class="basis-full flex-col lg:min-w-[30%]">

            <div class="flex justify-between text-white bg-slate-700 rounded-t-lg md:rounded-r-none lg:rounded-r-none p-2 text-sm">
              <div class="day">{{$current_data['r']}}</div>
            </div>

            <div class="p-2 ml-1 flex flex-col items-center md:items-start lg:items-start">
              <div class="place text-white mt-2">{{$current_data['timezone']}}</div>

              <div class="flex gap-6 items-center mt-2">
                <div class="temp text-white text-6xl font-bold">{{$current_data['temp']}}°C</div>

                <div class="sun">
                  <img src="http://openweathermap.org/img/wn//{{$current_data['icon']}}@4x.png" alt="weather icon" class="w-icon">
                  
                </div>
              </div>
              <div class="place text-white ">Feels like: {{$current_data['feels_like']}}°C</div>
              <div class="place text-white ">Humidity: {{$current_data['humidity']}}%</div>
        

              <div class="flex gap-6 mt-2">
                <div class="flex mt-2 gap-1 items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sunset text-white" viewBox="0 0 16 16">
                   <path d="M7.646 4.854a.5.5 0 0 0 .708 0l1.5-1.5a.5.5 0 0 0-.708-.708l-.646.647V1.5a.5.5 0 0 0-1 0v1.793l-.646-.647a.5.5 0 1 0-.708.708l1.5 1.5zm-5.303-.51a.5.5 0 0 1 .707 0l1.414 1.413a.5.5 0 0 1-.707.707L2.343 5.05a.5.5 0 0 1 0-.707zm11.314 0a.5.5 0 0 1 0 .706l-1.414 1.414a.5.5 0 1 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zM8 7a3 3 0 0 1 2.599 4.5H5.4A3 3 0 0 1 8 7zm3.71 4.5a4 4 0 1 0-7.418 0H.499a.5.5 0 0 0 0 1h15a.5.5 0 0 0 0-1h-3.79zM0 10a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2A.5.5 0 0 1 0 10zm13 0a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                  </svg>
                  <div class="text-white">{{$current_data['sunrise']}}</div>
                </div>
                
                <div class="flex mt-2 gap-1 items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sunset text-white" viewBox="0 0 16 16">
                    <path d="M7.646 4.854a.5.5 0 0 0 .708 0l1.5-1.5a.5.5 0 0 0-.708-.708l-.646.647V1.5a.5.5 0 0 0-1 0v1.793l-.646-.647a.5.5 0 1 0-.708.708l1.5 1.5zm-5.303-.51a.5.5 0 0 1 .707 0l1.414 1.413a.5.5 0 0 1-.707.707L2.343 5.05a.5.5 0 0 1 0-.707zm11.314 0a.5.5 0 0 1 0 .706l-1.414 1.414a.5.5 0 1 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zM8 7a3 3 0 0 1 2.599 4.5H5.4A3 3 0 0 1 8 7zm3.71 4.5a4 4 0 1 0-7.418 0H.499a.5.5 0 0 0 0 1h15a.5.5 0 0 0 0-1h-3.79zM0 10a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2A.5.5 0 0 1 0 10zm13 0a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                  </svg>
                  <div class="text-white">{{$current_data['sunset']}}</div>
                </div>

                <div class="flex mt-2 gap-1 items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-wind text-white " viewBox="0 0 16 16">
                    <path
                      d="M12.5 2A2.5 2.5 0 0 0 10 4.5a.5.5 0 0 1-1 0A3.5 3.5 0 1 1 12.5 8H.5a.5.5 0 0 1 0-1h12a2.5 2.5 0 0 0 0-5zm-7 1a1 1 0 0 0-1 1 .5.5 0 0 1-1 0 2 2 0 1 1 2 2h-5a.5.5 0 0 1 0-1h5a1 1 0 0 0 0-2zM0 9.5A.5.5 0 0 1 .5 9h10.042a3 3 0 1 1-3 3 .5.5 0 0 1 1 0 2 2 0 1 0 2-2H.5a.5.5 0 0 1-.5-.5z" />
                  </svg>
                  <div class="text-white">{{$current_data['wind_speed']}}km/h</div>
                </div>

                
              </div>
            </div>
          </div>

          <!-- Tuesday -->
          @foreach($forecast_data as $key=>$value)
          @if($key%2!=0 && $key!=0)
          <div class="bg-slate-700 basis-full">
          
            <div class="flex justify-center text-white bg-slate-800 p-2 text-sm">
              <div class="day">{{getdate($value->dt)['weekday']}}</div>
            </div>
          
            <div class="p-2 ml-1">
          
              <div class="flex gap-2 items-center mt-2 flex-col">
                <div class="cloud-sun">
                  <img src="http://openweathermap.org/img/wn//{{$value->weather[0]->icon}}@4x.png" alt="weather icon" class="w-icon ">
                </div>
                <div class="temp text-white text-3xl font-bold">{{$value->temp->max}}°C</div>
                <div class="temp text-slate-300 text-md font-bold">{{$value->temp->min}}°C</div>
              </div>
          
            </div>
          </div>
          @elseif($key%2==0 && $key!=0)
           <div class="bg-slate-600 basis-full">
          
            <div class="flex justify-center text-white bg-slate-800 md:bg-slate-700 lg:bg-slate-700 p-2 text-sm">
              <div class="day">{{getdate($value->dt)['weekday']}}</div>
            </div>
          
            <div class="p-2 ml-1">
          
              <div class="flex gap-2 items-center mt-2 flex-col">
                <div class="cloud">
                  <img src="http://openweathermap.org/img/wn//{{$value->weather[0]->icon}}@4x.png" alt="weather icon" class="w-icon ">
                </div>
                <div class="temp text-white text-3xl font-bold">{{$value->temp->max}}°C</div>
                <div class="temp text-slate-300 text-md font-bold">{{$value->temp->min}}°C</div>
              </div>
          
            </div>
          </div>
          @else
          @endif
          @endforeach
          

        </div>