<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Designed by Shamiul">
  <script src="https://cdn.tailwindcss.com"></script>
  
</head>

<body style="min-width: 375px;">
  
  <main class="md:h-screen lg:h-screen h-[100%] min-w-[100%] bg-gradient-to-r from-slate-900 to-indigo-900">
    
    @include('header')

    <div class="pl-8 pr-8 pt-12">
      
      <div class="main flex justify-center" id="search_ber">
        @include('weather.search_ber')
      </div>

      <div class="main flex md:flex lg:flex md:justify-center lg:justify-center pl-8 pr-8 pt-12 pb-12" id="weather_report">
       
      </div>

    </div>

  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript" src="/js/weather.js"></script>
  

</body>

</html>