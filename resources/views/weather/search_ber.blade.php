<div class="search p-8 flex justify-center relative max-w-4xl basis-full">
  
  <select class="rounded-full p-3 w-full focus:outline-slate-900 bg-slate-800 text-white" name="prefecture" id="prefecture">
    @foreach($prefectures as $key=>$ken)
    <option value="{{$ken}}" @if($current_city==$ken) selected @endif>{{$ken}}</option>
    @endforeach
  </select>
  
</div>


