<header class="bg-slate-700">
      
      <div class="flex items-center justify-between p-2 text-white">
        <div class="logo flex items-center gap-1">
          <!-- <img src="img/logo.png" alt="" srcset="" class="h-16"> -->
          <a href="{{route('homepage')}}">
          <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="currentColor" class="bi bi-tree p-1"
            viewBox="0 0 16 16">
            <path
              d="M8.416.223a.5.5 0 0 0-.832 0l-3 4.5A.5.5 0 0 0 5 5.5h.098L3.076 8.735A.5.5 0 0 0 3.5 9.5h.191l-1.638 3.276a.5.5 0 0 0 .447.724H7V16h2v-2.5h4.5a.5.5 0 0 0 .447-.724L12.31 9.5h.191a.5.5 0 0 0 .424-.765L10.902 5.5H11a.5.5 0 0 0 .416-.777l-3-4.5zM6.437 4.758A.5.5 0 0 0 6 4.5h-.066L8 1.401 10.066 4.5H10a.5.5 0 0 0-.424.765L11.598 8.5H11.5a.5.5 0 0 0-.447.724L12.69 12.5H3.309l1.638-3.276A.5.5 0 0 0 4.5 8.5h-.098l2.022-3.235a.5.5 0 0 0 .013-.507z" />
              </a>
          </svg>
           
        </div>
        <nav class="nav-ham hidden list-none lg:flex lg:gap-4 md:flex md:gap-4">
          <li>
            <a href="{{route('homepage')}}" class="rounded-full pl-4 pr-4 pt-0 pb-1  hover:bg-slate-100 hover:text-slate-900 hover:transition duration-300 @if(isset($page) && $page=='home')ring-2 @else @endif">Home</a>
          </li>
          <li>
            <a href="{{route('search.bar')}}" class="rounded-full pl-4 pr-4 pt-0 pb-1 hover:bg-slate-100 hover:text-slate-900 hover:transition duration-300 @if(isset($page) && $page=='place')ring-2 @else @endif">Place Search</a>
          </li>
          
        </nav>
        <div class="nav-list block text-blue-100 pr-1 md:hidden lg:hidden hover:cursor-pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-list"
            viewBox="0 0 16 16">
            <path fill-rule="evenodd"
              d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
          </svg>
        </div>
        <div class="nav-close hidden block text-blue-100 pr-1 md:hidden lg:hidden hover:cursor-pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
            <path
              d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </div>
      </div>
      
      <nav class="nav-menu hidden text-blue-100 list-none flex flex-col items-center gap-4 pt-3 pb-4">
        <li><a href="{{route('homepage')}}" class="rounded-full pl-4 pr-4 pt-0 pb-1 hover:font-bold hover:transition duration-300">Home</a></li>
        <li><a href="{{route('search.bar')}}" class="rounded-full pl-4 pr-4 pt-0 pb-1 hover:font-bold hover:transition duration-300">Place Search</a></li>
      </nav>

    </header>