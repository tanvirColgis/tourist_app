<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    
    <title>Weather App</title>
</head>
<body>
    
    <div class="container">
      <div>
        <td>Weather Forecast</td>
        <td>Location search</td>
      </div>
      <div>
        <select name='city' class="form-select">
          <option>tokyo</option>
          <option>tokyo</option>
          <option>tokyo</option>
          <option>tokyo</option>
          <option>tokyo</option>
        </select>
      </div>
        <div class="current-info" id="current-info">
         {!! $current_html !!}
        </div>
    </div>
    <div class="future-forecast" id="future-forecast"> 
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    
    <script src="script.js"></script>
</body>
</html>