**You can see the demonstration here:** [web view](http://bizmates-tourist-app.herokuapp.com/)

It may take some time to load initially because it is a free tier of Heroku.

**To run the system locally follow the instruction:**

1. Download the repository in the local system.
2. Run the command `composer update`.
3. After composer update run `php artisan serve`.

**Language and framework:**

1. PHP: ^7.3|^8.0 (backend)
2. Laravel: 8.75 (backend)
3. Javascript (frontend)
4. Jquery 3.5.1 (frontend)
4. TailwindCss (design)


**API used for the development of thisApplication:**

In total 5 APIs have been used. they are:

1. Open weather map API: This one is used to get the weather forecast. Detailed documentation can be found  [here](https://openweathermap.org/api)

2. foursquare API: This API is used to get the nearby places in a specific prefecture. Here is the documentation [here](https://developer.foursquare.com/reference/places-nearby) 

3. Google Maps JavaScript API: Google Map API is used to indicate the place with marks in a map to visualize the information in a better way. This is the documentation link [here](https://developers.google.com/maps/documentation/javascript/overview)

4. Positionstack API: Used to get the latitude and longitude of a place based on prefecture name.[documentation](urhttps://positionstack.com/documentationl)
5. IP Geolocation API: This API is used to get the prefecture name from the IP address of the user. To use these API sign up or any API keys are not needed.[documentation](https://ip-api.com/) 


**Application's Brief Description:**

This is a simple information app for foreign tourists.
On the home page, it will show the weather forecast of the user's current prefecture.
Users can also know the weather condition of other prefectures of Japan. 
Information will be given for the next one-week forecast also. 
There is another module for searching venus of the specific prefecture.
Anyone can easily select the type of place/venue and prefecture. 
Listing of places will show and indication of those places in google maps.
To know the details of a specific place after clicking one new window will open.
It will describe the exact address and some photos(with date) of that venue. 
