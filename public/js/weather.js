$(document).ready(function () {
  // Show Menu
   $(".nav-list").click(function () {
     $(this).hide();
     $(".nav-close").fadeIn();
     $(".nav-menu").removeClass("hidden");
   });
   // Hide Menu
   $(".nav-close").click(function () {
     $(this).hide();
     $(".nav-list").fadeIn();
     $(".nav-menu").addClass("hidden");
   });
   
   //call forecast data through API
   weather_data();
 });
    

$('#prefecture').on('change', function() {
  weather_data()
});

function weather_data() {
	
	var selected_prefecture=$("#prefecture").val()

	var request = $.ajax({
          url: "api/weather_forecast",
          method: "POST",
          data: { id : selected_prefecture },
          dataType: "html"
    });
    
    request.done(function( msg ) {
       $( "#weather_report" ).html( msg );
    });
 
    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });

}