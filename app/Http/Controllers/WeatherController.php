<?php

namespace App\Http\Controllers;
use Config;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public static function index()
    {
        // geting the prefectures name from config/ken.php file
        $prefectures=Config::get('ken');

        // declare this variable to track the homepage
        $page='home';
        
        // retriving IP address of the user
        $ip = getenv('HTTP_CLIENT_IP')?:
              getenv('HTTP_X_FORWARDED_FOR')?:
              getenv('HTTP_X_FORWARDED')?:
              getenv('HTTP_FORWARDED_FOR')?:
              getenv('HTTP_FORWARDED')?:
              getenv('REMOTE_ADDR');
 
        if($ip==false){
          $ip='54.65.87.243'; // if IP is not detected then  set IP of Tokyo
        } 
        
        // Using ip address retriving prefecture name
        $ip_array= self::get_ip_detail($ip);
        $current_city=$ip_array->regionName;
 
        return view('weather.index',compact('prefectures','page','current_city'));
    } 

    private static function get_ip_detail($ip){
        $ip_response = file_get_contents('http://ip-api.com/json/'.$ip);
        $ip_array=json_decode($ip_response);
        return  $ip_array;
    }

    public function vur_page(){

        return view('vue_page');
    }
}
