<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

class PlaceController extends Controller
{
    public static function index()
    {
        // geting the prefectures name from config/ken.php file
        $prefectures=Config::get('ken');

        // declare this variable to track the place searching page
        $page='place';
        
        // retriving IP address of the user
        $ip = getenv('HTTP_CLIENT_IP')?:
              getenv('HTTP_X_FORWARDED_FOR')?:
              getenv('HTTP_X_FORWARDED')?:
              getenv('HTTP_FORWARDED_FOR')?:
              getenv('HTTP_FORWARDED')?:
              getenv('REMOTE_ADDR');
 
        if($ip==false){
          $ip='54.65.87.243'; // if IP is not detected then  set IP of Tokyo
        } 
        
        // Using ip address retriving prefecture name
        $ip_array= self::get_ip_detail($ip);
        $current_city=$ip_array->regionName;
 
        return view('place.place_index',compact('prefectures','page','current_city'));
        
    }

    public static function place_list(Request $request)
    {
        
        //call gazzle package
        $client = new \GuzzleHttp\Client();

        // selected prefecture
        $prefecture = isset($request['prefecture'])?$request['prefecture']:'Tokyo' ;

        //get latitude and longitude of the selected prefecture

        $api_url='http://api.positionstack.com/v1/forward?access_key='.env('LAT_LON_API_KEY').'&query='.$prefecture;
        
        $response = $client->request('GET', $api_url, [
          'headers' => [
            'Accept' => 'application/json',
          ],
        ]);
        
        $json_response=json_decode($response->getBody());
        $lat=$json_response->data[0]->latitude;
        $lan=$json_response->data[0]->longitude;
        
        $topic_from_diff_button=!empty(request('button_topic'))?request('button_topic'):'food';
        $topic= !empty(request('topic'))?request('topic'):$topic_from_diff_button;
        
        //API url
        $api_url_foursquare='https://api.foursquare.com/v3/places/search?query='.$topic.'&ll='.$lat.'%2C'.$lan;
      
        $response = $client->request('GET', $api_url_foursquare, [
          'headers' => [
            'Accept' => 'application/json',
            'Authorization' => env('PLACE_API_KEY'), //fetching the api key from env file
          ],
        ]);
        
        $places=json_decode($response->getBody())->results;
       
        return view('place.searched_places',compact('places','lat','lan'));
    }

    public function place_detail($id)
    {

      //call gazzle package
      $client = new \GuzzleHttp\Client();
      
      // get place detail through api
      $response = $client->request('GET', 'https://api.foursquare.com/v3/places/'.$id, [
        'headers' => [
          'Accept' => 'application/json',
          'Authorization' => env('PLACE_API_KEY'), //fetching the api key from env file
        ],
      ]);

      $place_description=json_decode($response->getBody());
    
      // get place images through api
      $response = $client->request('GET', 'https://api.foursquare.com/v3/places/'.$id.'/photos', [
        'headers' => [
          'Accept' => 'application/json',
          'Authorization' => env('PLACE_API_KEY'), //fetching the api key from env file
        ],
      ]);

      $place_photos=json_decode($response->getBody());

      return view('place.place_detail',compact('place_description','place_photos'));
    }

    private static function get_ip_detail($ip){
        $ip_response = file_get_contents('http://ip-api.com/json/'.$ip);
        $ip_array=json_decode($ip_response);
        return  $ip_array;
    }
}
