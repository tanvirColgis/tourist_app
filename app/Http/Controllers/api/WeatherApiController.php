<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WeatherResource;
use Illuminate\Http\Request;

class WeatherApiController extends Controller
{
    public function forecast_data(Request $request)
    {
        //call gazzle package
        $client = new \GuzzleHttp\Client();

        // selected prefecture
        $prefecture = isset($request['id'])?$request['id']:'Tokyo' ;

        //get latitude and longitude of the selected prefecture

        $api_url='http://api.positionstack.com/v1/forward?access_key='.env('LAT_LON_API_KEY').'&query='.$prefecture;
        
        $response = $client->request('GET', $api_url, [
          'headers' => [
            'Accept' => 'application/json',
          ],
        ]);
        
        $json_response=json_decode($response->getBody());
        $latitude=$json_response->data[0]->latitude;
        $longitude=$json_response->data[0]->longitude;
       
        
        
        // get weather data from API
        $weather_api_url='https://api.openweathermap.org/data/2.5/onecall?lat='.$latitude.'&lon='.$longitude.'&exclude=hourly,minutely&units=metric&appid='.env('WEATHER_API_KEY');

        $response = $client->request('GET', $weather_api_url, [
          'headers' => [
            'Accept' => 'application/json',
          ],
        ]);

        $data=json_decode($response->getBody());
        
        //prepare today's data
        $current_data=  [
            'sunrise'=> date('H:i', $data->current->sunrise),
            'sunset'=> date('H:i', $data->current->sunset),
            'temp' => $data->current->temp,
            'main' => $data->current->weather[0]->main,
            'description' => $data->current->weather[0]->description,
            'icon' => $data->current->weather[0]->icon,
            'feels_like' => $data->current->feels_like,
            'pressure' => $data->current->pressure,
            'humidity' => $data->current->humidity,
            'clouds' => $data->current->clouds,
            'wind_speed' => $data->current->wind_speed,
            'day' => date('d', $data->current->dt),
            'r' => date('r', $data->current->dt),
            'timezone' => $data->timezone,

        ];
        // forecasted data
        $forecast_data=$data->daily;
   
        // get hetml response
        $forecast_html = view('weather.report', compact('current_data','forecast_data'))->render();
        
        // send html for showing
        return $forecast_html;
        
        
    }
}
