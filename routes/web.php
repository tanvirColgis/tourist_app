<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeatherController;
use App\Http\Controllers\PlaceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/',[WeatherController::class, 'index'])->name('homepage');
Route::get('/place_search',[PlaceController::class, 'index'])->name('search.bar');
Route::post('/place_list',[PlaceController::class, 'place_list'])->name('search.place');
Route::get('/place_detail/{id}',[PlaceController::class, 'place_detail'])->name('place.detail');

Route::get('/vue_test',[WeatherController::class, 'vur_page']);
Route::get('/input',[WeatherController::class, 'vur_page'])->name('input');
Route::get('/ariticle',[WeatherController::class, 'vur_page'])->name('ariticle');

Route::get('/{any}', [WeatherController::class, 'vur_page'])->where('any', '.*');